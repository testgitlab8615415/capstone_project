tatLoading();
idProductUpdate=null;
function fetchProduct(){
  batLoading();                                             
  productServ
  .getList()
  .then(function(res){
  renderProduct(res.data);
  tatLoading();
  })
  .catch(function(err){
    tatLoading();
  }
  )
}
fetchProduct();
function themSP(){
    tatLoading();
    var newProduct=layThongTinTuForm();
    var isValid=checkNumber("tbPrice","number only! & 1M-50M",newProduct.price)&checkName("tbName","text only!",newProduct.name)&checkType("tbType","Samsung and Iphone only!",newProduct.type);
    if(isValid){
  productServ
  .create(newProduct)
  .then(function(res){
    fetchProduct();
    document.getElementById("formBody").reset();

  })
  .catch(function(err){});
  }}
  function deleteSP(id){
    batLoading();
    productServ
    .delete(id)
    .then(function(res){
      fetchProduct();
      renderProduct(res.data);
      tatLoading();
    })
    .catch(function(err){
    
     tatLoading();

    })
    
  }
  function showInformation(id){
batLoading();
    idProductUpdate=id;
    productServ
    .getID(id)
    .then(function(res){
      showThongTinLenForm(res.data);
      tatLoading();
    }
    ).catch(function(err){
     tatLoading();
    })
  
  }
 function capNhatSP(){
   $("#exampleModal").modal("hide");
  batLoading();
    var product=layThongTinTuForm();
    productServ
    .update(idProductUpdate,product)
    .then(function(res){
      fetchProduct();
      tatLoading();
  }
 ).catch(function(err){
tatLoading();
 })
  }
  function searchSP(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderResult(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )

  }
  function sortAcending(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderAcending(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )

  }
  function sortDecending(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderDecending(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )

  }